<?php 

$servername = "localhost";
$username = "root";
$password = "";
$conn = new PDO("mysql:host=$servername;dbname=kiddocare", $username, $password);

function connection($query,$action){
    // global $servername,$username,$password;
    try{
        global $conn;
        $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        
        if($action == "select"){
         $stmt = $conn->prepare($query);
         $stmt->execute();
         $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
         return $result;
        }
        

        
    
    }
    catch(PDOEXception $e){

    //  echo "Error-hahahah: " . $e->getMessage();
     
    }

}

function totalSale($query){
    

    $data = connection($query,"select");

    // print_r(json_encode($data));


    $total = 0;

    foreach($data as $item){

       $unitPrice = $item["UnitPrice"];
       $quantity = $item["Quantity"];
       $discount = $item["Discount"];

       $priceBeforeDiscount = $unitPrice * $quantity;
       $priceAfterDiscount = $priceBeforeDiscount - ( $priceBeforeDiscount * ($discount/100)); 
      
       $total = $total + $priceAfterDiscount;


    // return $data;
}

return $total;
}

function totalOrder($query){

 $data = connection($query,"select_count");

 return $data[0]["COUNT(OrderID)"];
}



function select($query){
    $data = connection($query,"select");

    return $data;
}

?>


