<?php

session_start();


if (!isset($_SESSION["employee"])) {
    header("Location:login.php");
    exit;
}

require 'db.php';

// $totalSale = totalSale("SELECT * from order_details");



$dailySale = select("SELECT order_details.UnitPrice,order_details.Quantity,order_details.Discount,  DAY(orders.OrderDate)  As 'daily' FROM order_details JOIN orders On order_details.OrderID = orders.OrderID
Where YEAR(orders.OrderDate) = 1995 AND MONTH(orders.OrderDate) = 5 ");

$total = 0;
foreach ($dailySale as $item) {
    $unitPrice = $item["UnitPrice"];
    $Quantity = $item["Quantity"];
    $Discount = $item["Discount"];

    $priceBeforeDiscount = $unitPrice * $Quantity;
    $priceAfterDiscount = $priceBeforeDiscount - ($priceBeforeDiscount * ($Discount / 100));

    $total = $total + $priceAfterDiscount;
}

$totalSale = number_format($total, 2, '.', '');

$totalOrder = select("SELECT COUNT(OrderID) from orders WHERE YEAR(OrderDate) = 1995 AND MONTH(OrderDate) = 5 ");


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <title>Document</title>
</head>

<body>
    <div class="container p-5">
        <a href="logout.php" class="btn-sm btn-danger">log out</a>
        <h1>Sales Dashboard</h1>
        <h4>May 1995</h4>
        <select class="form-select" aria-label="Default select example" id="month" onchange="redrawChart()">
            <option selected>5</option>
           
        </select>

        <div class="row pt-4">
            <div class="col">
                <div class="card">

                    <div class="card-body">
                        <h5 class="card-title">Total Sales</h5>
                        <p class="card-text">RM <?php echo $totalSale ?></p>

                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">

                    <div class="card-body">
                        <h5 class="card-title">Total Orders</h5>
                        <p class="card-text"><?php echo ($totalOrder[0]["COUNT(OrderID)"]) ?></p>

                    </div>
                </div>
            </div>

        </div>

        <div id="chart-container" class="pt-4">
            <canvas id="mycanvas"></canvas>
        </div>


        <div class="row pt-4">

            <div class="col">
                <h4>Sales by Product Category</h4>
                <div id="chart-container">
                    <canvas id="paichart"></canvas>
                </div>
            </div>
            <div class="col">
                <h4>Sales by Customers</h4>
                <div id="chart-container">
                    <canvas id="bycustomer"></canvas>
                </div>
            </div>
            <div class="col">
                <h4>Sales by Employees</h4>
                <div id="chart-container">
                    <canvas id="byemployee"></canvas>
                </div>
            </div>
        </div>
    </div>



</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


<script>
    $(document).ready(function() {

        salesByProductCategory();
        salesByCustomer();
        dailySale();
        salesByEmployee();

    });

    function dailySale() {
        $.ajax({

            url: "dailysale.php",
            type: "GET",
            data: {
                month: 5
            },
            success: function(data) {
                //    console.log(data);

                var day = [];
                var total = [];

                for (var i in data) {
                    day.push("Day: " + data[i].day);
                    total.push(data[i].total);

                }

                var chartdata = {
                    labels: day,
                    datasets: [{
                        label: 'Daily Sales',
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 205, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(201, 203, 207, 0.2)'
                        ],
                        borderColor: [
                            'rgb(255, 99, 132)',
                            'rgb(255, 159, 64)',
                            'rgb(255, 205, 86)',
                            'rgb(75, 192, 192)',
                            'rgb(54, 162, 235)',
                            'rgb(153, 102, 255)',
                            'rgb(201, 203, 207)'
                        ],
                        borderWidth: 1,
                        // hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        // hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: total
                    }]
                };
                var ctx = $("#mycanvas");

                var barGraph = new Chart(ctx, {
                    type: 'bar',
                    data: chartdata
                });
            },

        })
    }


    function salesByProductCategory() {
        $.ajax({
            url: 'salesproductcategory.php',
            type: "GET",
            data: {
                month: 5
            },
            success: function(data) {
                //    console.log(data);

                var categoryName = [];
                var totalQuantity = [];

                for (var i in data) {
                    categoryName.push(data[i].categoryName);
                    totalQuantity.push(data[i].totalQuantity);

                }

                var chartdata = {
                    labels: categoryName,
                    datasets: [{
                        label: 'Daily Sales',
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 205, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(201, 203, 207, 0.2)'
                        ],
                        borderColor: [
                            'rgb(255, 99, 132)',
                            'rgb(255, 159, 64)',
                            'rgb(255, 205, 86)',
                            'rgb(75, 192, 192)',
                            'rgb(54, 162, 235)',
                            'rgb(153, 102, 255)',
                            'rgb(201, 203, 207)'
                        ],
                        borderWidth: 1,
                        // hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        // hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: totalQuantity
                    }]
                };
                var ctx = $("#paichart");

                var barGraph = new Chart(ctx, {
                    type: 'doughnut',
                    data: chartdata
                });
            },

        })
    }

    function salesByCustomer() {

        //setup block

        $.ajax({
            url: 'salesbycustomer.php',
            type: "GET",
            data: {
                month: 5
            },
            success: function(data) {
                //    console.log(data);

                var customerName = [];
                var salesNumber = [];

                for (var i in data) {
                    customerName.push(data[i].customerName);
                    salesNumber.push(data[i].salesNumber);

                }

                var chartdata = {
                    labels: customerName,
                    datasets: [{
                        axis: 'y',
                        label: 'Sale by Customer',
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 205, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(201, 203, 207, 0.2)'
                        ],
                        borderColor: [
                            'rgb(255, 99, 132)',
                            'rgb(255, 159, 64)',
                            'rgb(255, 205, 86)',
                            'rgb(75, 192, 192)',
                            'rgb(54, 162, 235)',
                            'rgb(153, 102, 255)',
                            'rgb(201, 203, 207)'
                        ],
                        borderWidth: 1,
                        // hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        // hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: salesNumber
                    }]
                };
                var ctx = $("#bycustomer");
        
             


                var barGraph = new Chart(ctx, {
                    type: 'bar',
                    data: chartdata,
                    options: {
                        indexAxis: 'x',
                        barPercentage: 1,
                        height: 500
                    }
                });
            },

        })
    }

    function salesByEmployee() {
        

        
        $.ajax({
            url: 'salesbyemployee.php',
            type: "GET",
            data: {
                month: 5
            },
            success: function(data) {
              

                var employeeName = [];
                var salesNumber = [];

                for (var i in data) {
                    employeeName.push(data[i].employeeName);
                    salesNumber.push(data[i].salesNumber);

                }

                var chartdata = {
                    labels: employeeName,
                    datasets: [{
                        axis: 'y',
                        label: 'Sale by Employee',
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 205, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(201, 203, 207, 0.2)'
                        ],
                        borderColor: [
                            'rgb(255, 99, 132)',
                            'rgb(255, 159, 64)',
                            'rgb(255, 205, 86)',
                            'rgb(75, 192, 192)',
                            'rgb(54, 162, 235)',
                            'rgb(153, 102, 255)',
                            'rgb(201, 203, 207)'
                        ],
                        borderWidth: 1,
                        // hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        // hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: salesNumber
                    }]
                };
                var ctx = $("#byemployee");
                // var ctx = document.getElementById('byemployee').getContext('2d'); // 2d context
             

                var barGraph = new Chart(ctx, {
                    type: 'bar',
                    data: chartdata,
                    options: {
                        indexAxis: 'y',
                        barPercentage: 1,

                    }
                });
                


                
            },

        })

       
    }

    function redrawChart() {

        // var month = $("#month").val();

   
        // $('#byemployee').remove();
        // salesByEmployee();
        // salesByCustomer();
        // dailySale();
        // salesByEmployee();
    }
</script>



</html>