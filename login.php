<?php

//start session
session_start();

//check session
if (isset($_SESSION["employee"])) {
    header("Location:index.php");
    exit;
}

if (isset($_POST["submit"])) {

    $nama = $_POST["nama"];
    $password = $_POST["katalaluan"];

    //get credential from txt file.
    $myfile = fopen("users.txt", "r") or die("Unable to open file!");
    $credential = fread($myfile, filesize("users.txt"));
    $pattern = "/$nama|$password/i";


    //credential validation
    if (preg_match_all($pattern, $credential, $matches)) {
        if (count($matches[0]) == 2) {
            //set session
            $_SESSION["employee"] = true;
            $_SESSION["nama"] = $staff["nama"];
            header("Location:index.php");
            exit;
        } else {
            echo "<div class='alert alert-danger m-5' role='alert'> Failed to login! </div>";
        }
    }
    fclose($myfile);
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <title>Login</title>
</head>

<body>
    <div class="container p-5">
        <h1>Log In</h1>
        <?php if (isset($error)) : ?>
            <p style="color:red;">Username/ password salah</p>
        <?php endif;  ?>
       
        <form action="" method="post">
            <div class="mb-3">
                <label for="name" class="form-label">Name:</label>
                <input type="text" name="nama" class="form-control" id="name" aria-describedby="emailHelp">
               
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Password: </label>
                <input type="password" name="katalaluan" class="form-control" id="exampleInputPassword1">
            </div>
           
            <button type="submit" name="submit" class="btn btn-primary">Log In</button>
        </form>
    </div>

</body>

</html>